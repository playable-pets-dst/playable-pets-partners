return

------------------------------------------------------------------
-- Variables
------------------------------------------------------------------

local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient

local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

local STRINGS = require("ppp_strings")


------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

PlayablePets.Init(env.modname)

local TUNING = require("ppp_tuning")

GLOBAL.PPP_FORGE = require("ppp_forge")


------------------------------------------------------------------
-- Prefabs
------------------------------------------------------------------

PrefabFiles = {
	"nightswordp_forge",
	-------FX--------

	-------Tools---------
	--"pmonster_wpn",
}

-- Centralizes a number of related functions about playable mobs to one convenient table
-- Name is the prefab name of the mob
-- Fancyname is the name of the mob, as seen in-game
-- Gender is fairly self-explanatory
-- Skins is a list of any custom mob skins that may be available, using the Modded Skins API by Fidoop
-- Skins are not required for mobs to function, but they will display a custom portrait when the mob is examined by a player
GLOBAL.PPP_MobCharacters = {
	berniep        = { fancyname = "Bernie",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true },
	abbyp        = { fancyname = "Abigail",           gender = "FEMALE",   mobtype = {}, skins = {}, forge = true },
	balloonp        = { fancyname = "Balloon",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true },
	smallghostp        = { fancyname = "Pipspook",           gender = "ROBOT",   mobtype = {}, skins = {}, forge = true },
	balloon_partyp        = { fancyname = "Party Balloon",           gender = "ROBOT",   mobtype = {}, skins = {} },
	balloon_speedp        = { fancyname = "Speed Balloon",           gender = "ROBOT",   mobtype = {}, skins = {} },
	--maxpuppetp        = { fancyname = "Maxwell's Shadow",           gender = "ROBOT",   mobtype = {}, skins = {} },
}

-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PPP_Character_Order = {"berniep", "abbyp", "balloonp", "balloon_partyp", "balloon_speedp", "smallghostp"}
------------------------------------------------------------------
-- Assets
------------------------------------------------------------------

Assets = {
	Asset("ANIM", "anim/ghost_monster_build.zip"), 
}

------------------------------------------------------------------
-- Custom Recipes
------------------------------------------------------------------


------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------

------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------
-------------------------------------------------------
--Wardrobe stuff--

--Skin Puppet Stuff
local MobPuppets = require("ppp_puppets")
local MobSkins = require("ppp_skins")
PlayablePets.RegisterPuppetsAndSkins(PPP_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPP_Character_Order) do
	local mob = GLOBAL.PPP_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	for _, skin in ipairs(mob.skins) do
			table.insert(PrefabFiles, prefab.."_"..skin)
	end
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end

------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------

-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PPP_Character_Order) do
	local mob = GLOBAL.PPP_MobCharacters[prefab]
	
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)
		PlayablePets.SetGlobalData(prefab)
		if GLOBAL.SKIN_RARITY_COLORS.ModMade ~= nil then
			GLOBAL.MakeModCharacterSkinnable(prefab)
		end
	end
end