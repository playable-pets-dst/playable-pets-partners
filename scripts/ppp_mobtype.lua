local MOBTYPE =
{
	NORMAL = 1,
	CRAFTY = 2,
	GIANT = 3,
	BOSS = 4,
}

return MOBTYPE