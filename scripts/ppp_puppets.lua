local DEFAULT_SCALE = 0.20
local Puppets = {	
	--Partner
	berniep = {bank = TheNet:GetServerGameMode() == "lavaarena" and "bernie_big" or "bernie", build = "bernie_build", anim = "idle_loop_nodir", scale = TheNet:GetServerGameMode() == "lavaarena" and (DEFAULT_SCALE - 0.1) or DEFAULT_SCALE, shinybuild = "bernie" },
	abbyp = {bank = "ghost", build = "ghost_abigail_build", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "abby", uiscale = DEFAULT_SCALE - 0.05, y_offset = -10 },
	smallghostp = {bank = "ghost_kid", build = "ghost_kid", anim = "idle", scale = DEFAULT_SCALE, shinybuild = "smallghost", y_offset = -20 },
	balloonp = {bank = "balloon", build = "balloon", anim = "idle", scale = DEFAULT_SCALE- 0.05, shinybuild = "balloon", faceoverride = 0, override_build = "balloon_shapes", symbols_to_override = {"swap_balloon"}, symbols_to_override_with = {"balloon_1"} },
	balloon_partyp = {bank = "balloon2", build = "balloon2", anim = "idle", scale = DEFAULT_SCALE- 0.05, shinybuild = "balloon", faceoverride = 0, override_build = "balloon_shapes_party", symbols_to_override = {"swap_balloon"}, symbols_to_override_with = {"balloon_1"} },
	balloon_speedp = {bank = "balloon2", build = "balloon2", anim = "idle", scale = DEFAULT_SCALE- 0.05, shinybuild = "balloon", faceoverride = 0, override_build = "balloon_shapes_speed", symbols_to_override = {"swap_balloon"}, symbols_to_override_with = {"balloon_4"} },
}

return Puppets