return {
	OFF = "Off",
	DISABLE = "Disable",
	ENABLE = "Enable",
	LOCKED = "Locked",
	
	-- Mob Presets
	ALL_MOBS = "AllMobs",
	NO_GIANTS = "NoGiants",
	NO_BOSSES = "NoBosses",
	BOSSES_ONLY = "BossesOnly",
	GIANTS_ONLY = "GiantsOnly",
	CRAFTY_MOBS = "CraftyMobsOnly",
	
	-- Mob Houses
	HOUSE_CRAFT_ONLY = "Enable2",
	HOUSE_ON_SPAWN = "Enable1",
	HOUSE_BOTH = "Enable3",
	
	-- PvP Damage
	PVP_50_PERCENT_DMG = 0.5,
	PVP_100_PERCENT_DMG = 1.0,
	PVP_150_PERCENT_DMG = 1.5,
	PVP_200_PERCENT_DMG = 2.0,
	
	-- Misc
	HUMANOID_SANITY_ONLY = "Disable1",
	MONSTER_CHARCHANGE_ONLY = "Enable1",
}