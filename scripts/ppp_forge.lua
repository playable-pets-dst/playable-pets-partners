
return	{
	
	BERNIE = {
	
		HEALTH = 600,
	
		DAMAGE = 50,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 3,
		HIT_RANGE = 3,
	
		RUNSPEED = 6,
		WALKSPEED = 2,
		
		SPECIAL_CD = 10,
	},
	
	ABBY = {
	
		HEALTH = 600,
	
		DAMAGE = 10,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 3,
		HIT_RANGE = 3,
	
		RUNSPEED = 6,
		WALKSPEED = 2,
		
		SPECIAL_CD = 10,
	},
	
	GHOST_KID = {
	
		HEALTH = 125,
	
		DAMAGE = 5,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 2,
	
		RUNSPEED = 8,
		WALKSPEED = 6,
		
		SPECIAL_CD = 10,
	},
	
	MAXPUPPET = {
	
		HEALTH = 125,
	
		DAMAGE = 50,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 2,
		HIT_RANGE = 2,
	
		RUNSPEED = 8,
		WALKSPEED = 6,
		
		SPECIAL_CD = 10,
	},
	
	BALLOON = {
	
		HEALTH = 1,
	
		DAMAGE = 99999,
		ATTACKPERIOD = 0,
		
		ATTACK_RANGE = 3,
		HIT_RANGE = 3,
	
		RUNSPEED = 7,
		WALKSPEED = 7,
		
		SPECIAL_CD = 10,
	},
	--======================================
	--				Weapons
	--======================================
	--POOPWEAPON = {
	
		--DAMAGE = 30,
		--ALT_DAMAGE = 30,
		
	--},
}