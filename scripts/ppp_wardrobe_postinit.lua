local SkinsPuppet = require "widgets/skinspuppet" --by the time this is called, it should already be populated with necessary data
local oldSkinsPuppet_SetSkins = SkinsPuppet.SetSkins

local CB_Offset = -30
local PB_Offset = -20

function SkinsPuppet:SetSkins(prefabname, base_item, clothing_names, skip_change_emote, skinmode)
	if self.pp_puppets[prefabname] and not self.pp_puppets[prefabname].dontuse then
		local puppet = self.pp_puppets[prefabname]
		self.noemotes = true
		self.enable_idle_emotes = false
		self.play_non_idle_emotes = false
		self.animstate:ClearAllOverrideSymbols() --ideally should unhide any symbols and undo any swap changes
		--local pos = self.anim:GetPosition()
		self.anim:SetPosition(puppet.x_offset and puppet.x_offset or 0, puppet.y_offset and puppet.y_offset or 0)
		if puppet.sixfaced then
			self.anim:SetFacing(puppet.faceoverride and puppet.faceoverride or 6)
		else
			self.anim:SetFacing(puppet.faceoverride and puppet.faceoverride or 3)
		end
		if puppet.override_build then
			if puppet.symbols_to_override and puppet.symbols_to_override_with then
				for i, v in ipairs(puppet.symbols_to_override) do
					self.animstate:OverrideSymbol(v, puppet.override_build, puppet.symbols_to_override_with[i])
				end
			else
				self.animstate:AddOverrideBuild(puppet.override_build)
			end
		end
		
		if puppet.hide then
			for i, v in ipairs(puppet.hide) do
				self.animstate:Hide(v)
			end
		end
		self.looping = true
		self.animstate:SetBank(puppet.bank)
		self.animstate:SetBuild(puppet.build)
		self.anim:SetScale(puppet.scale)
		if not self.animstate:IsCurrentAnimation(puppet.anim) then
			self.animstate:PlayAnimation(puppet.anim, true)
		end
	else
		self.anim:SetFacing(3)
		self.noemotes = true --possible fix
		self.play_non_idle_emotes = true
		self.enable_idle_emotes = true
		self.anim:SetScale(0.20)
		self.anim:SetPosition(0, 0)
		oldSkinsPuppet_SetSkins(self, prefabname, base_item, clothing_names, skip_change_emote, skinmode)
	end
end

---------------------------------------------------------
--Icon fix--
local CharacterButton = require "widgets/redux/characterbutton"
local PlayerBadge = require "widgets/playerbadge"
local old_SetUpHead = CharacterButton._SetUpHead
local old_SetCharacter = CharacterButton.SetCharacter
local old_Set = PlayerBadge.Set

function CharacterButton:_SetUpPPHead(hero)	
	if SkinsPuppet.pp_puppets[hero] and not SkinsPuppet.pp_puppets[hero].dontuse then
		local puppet = SkinsPuppet.pp_puppets[hero]
		self.head_anim:SetPosition(puppet.x_offset and puppet.x_offset or 0, puppet.y_offset and (CB_Offset + puppet.y_offset) or CB_Offset)
		if puppet.sixfaced then
			self.head_anim:SetFacing(puppet.faceoverride and puppet.faceoverride or 6)
		else
			self.head_anim:SetFacing(puppet.faceoverride and puppet.faceoverride or 3)
		end
		if puppet.override_build then
			if puppet.symbols_to_override and puppet.symbols_to_override_with then
				for i, v in ipairs(puppet.symbols_to_override) do
					self.head_animstate:OverrideSymbol(v, puppet.override_build, puppet.symbols_to_override_with[i])
				end
			else
				self.head_animstate:AddOverrideBuild(puppet.override_build)
			end
		end
		
		if puppet.hide then
			for i, v in ipairs(puppet.hide) do
				self.head_animstate:Hide(v)
			end
		end
		self.looping = true
		self.head_animstate:SetBank(puppet.bank)
		self.head_animstate:SetBuild(puppet.build)
		self.head_anim:SetScale(puppet.uiscale and puppet.uiscale or puppet.scale)
		if not self.head_animstate:IsCurrentAnimation(puppet.anim) then
			self.head_animstate:PlayAnimation(puppet.anim, true)
		end
	end
end

function PlayerBadge:_SetUpPPHead(hero)	
	if SkinsPuppet.pp_puppets[hero] and not SkinsPuppet.pp_puppets[hero].dontuse then
		local puppet = SkinsPuppet.pp_puppets[hero]
		self.head_animstate:ClearAllOverrideSymbols() --ideally should unhide any symbols and undo any swap changes
		self.head_anim:SetPosition(puppet.x_offset and puppet.x_offset or 0, puppet.y_offset and (PB_Offset + puppet.y_offset) or PB_Offset)
		if puppet.sixfaced then
			self.head_anim:SetFacing(puppet.faceoverride and puppet.faceoverride or 6)
		else
			self.head_anim:SetFacing(puppet.faceoverride and puppet.faceoverride or 3)
		end
		if puppet.override_build then
			if puppet.symbols_to_override and puppet.symbols_to_override_with then
				for i, v in ipairs(puppet.symbols_to_override) do
					self.head_animstate:OverrideSymbol(v, puppet.override_build, puppet.symbols_to_override_with[i])
				end
			else
				self.head_animstate:AddOverrideBuild(puppet.override_build)
			end
		end
		
		if puppet.hide then
			for i, v in ipairs(puppet.hide) do
				self.head_animstate:Hide(v)
			end
		end
		self.looping = true
		self.head_animstate:SetBank(puppet.bank)
		self.head_animstate:SetBuild(puppet.build)
		self.head_anim:SetScale(puppet.uiscale and puppet.uiscale/1.5 or puppet.scale/1.5)
		self.head_animstate:PlayAnimation(puppet.anim, true)
	end
end

function PlayerBadge:Set(prefab, colour, ishost, userflags, base_skin)
	old_Set(self, prefab, colour, ishost, userflags, base_skin)
	if SkinsPuppet.pp_puppets[prefab] then
		PlayerBadge._SetUpPPHead(self, prefab)
	end
end

--[[
function PlayerBadge:UseAvatarImage()
    return self:IsAFK() or self.prefabname == "" or (self.ishost and not TheNet:GetServerIsClientHosted()) or SkinsPuppet.pp_puppets[self.prefabname]
end]]

function CharacterButton:SetCharacter(hero)
	self.herocharacter = hero
    --print("Overriding character button - "..tostring(hero))
	self.head_animstate:ClearAllOverrideSymbols() --ideally should unhide any symbols and undo any swap changes
    if SkinsPuppet.pp_puppets[hero] and not SkinsPuppet.pp_puppets[hero].dontuse then
		--print("Running Puppet override")
		self._SetUpPPHead(self, hero)
	else
		--print("Running default")
		self.head_anim:SetFacing(1)
		self.head_anim:SetPosition(0, 0)
		self.head_animstate:SetBank("wilson")
		self.head_animstate:PlayAnimation("idle_loop_ui", true)
		old_SetCharacter(self, hero)
	end
end