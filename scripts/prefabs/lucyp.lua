local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--This is a template for nerds.
---------------------------

--Don't add assets unless absolutely needed.
local assets = 
{

}

local prefabs = 
{	

}

local start_inv = 
{
	--"prefab",
}

-----------------------
local mob = 
{
	health = 9999,
	hunger = 9999,
	hungerrate = 0,
	sanity = 9999,
	runspeed = 8,
	walkspeed = 8,
	damage = 50,
	range = 3,
	bank = "lavaarena_lucy",
	build = "lavaarena_lucy",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGlucyp",
	minimap = "lucyp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable('lucyp',
-----Prefab---------------------Chance------------
{

})

local function EquipHat(inst) --Lucy will totally lose her head without this.
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if head then
		inst.AnimState:Show("HEAD")
	end 
end



-------------------------------------------------------
----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

------------------------------------------------------
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local function RestoreNightImmunity(inst) 
	inst.components.grue:AddImmunity("charlies_waifu")
end

local function setChar(inst) --sets character when the player loads.
	if not inst:HasTag("playerghost") then
    inst.AnimState:SetBank(mob.bank)
    inst.AnimState:SetBuild(mob.build)
    inst:SetStateGraph(mob.stategraph)
	end
	--if MONSTERHUNGER == "Disable" then
		inst.components.hunger:Pause()
	--end
	if MOBPVPMODE == "Enable" then
		PvPTeleport(inst)
	end
	inst.components.sanity.ignore = true
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
	inst.components.locomotor.fasteronroad = false	
end

local function RemovePenalty(inst)
	inst.components.health:DeltaPenalty(-1.00) --Removes health penalty when reviving
	inst.components.health:DoDelta(20000, false) --Returns health to max health upon reviving
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	--inst:WatchWorldState( "isday", function() SetNightVision(inst) end)
  	--inst:WatchWorldState( "isdusk", function() SetNightVision(inst) end)
  	--inst:WatchWorldState( "isnight", function() SetNightVision(inst)  end)
	--inst:WatchWorldState( "iscaveday", function() SetNightVision(inst) end)
  	--inst:WatchWorldState( "iscavedusk", function() SetNightVision(inst) end)
  	--inst:WatchWorldState( "iscavenight", function() SetNightVision(inst)  end)
	
	--SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
end

local function DontTriggerCreep(inst)
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
end


local master_postinit = function(inst)
	------------------------------------------
	--Stats--
    inst.components.health:SetMaxHealth(mob.health)
	inst.components.health:StartRegen(5, 5)
	inst.components.hunger:SetMax(mob.hunger)
	inst.components.sanity:SetMax(mob.sanity)
	inst.components.hunger:SetRate(mob.hungerrate)
	------------------------------------------
	--Combat--
	inst.components.combat.playerdamagepercent = MOBPVP
	inst.components.combat:SetAttackPeriod(0)
	inst.components.combat:SetRange(mob.range)
	inst.components.combat:SetDefaultDamage(mob.damage) --does double damage against mobs.
	--inst.components.combat:SetHurtSound("dontstarve_DLC001/creatures/vargr/hit")
	
	
	
    inst.components.temperature.inherentinsulation = -TUNING.INSULATION_SMALL
	inst.AnimState:SetBank(mob.bank)
	inst.AnimState:SetBuild(mob.build)
	
	inst.OnSetSkin = function(skin_name)
		inst.AnimState:SetBuild(mob.build)
		inst:SetStateGraph(mob.stategraph)
	
		inst:ListenForEvent("ms_respawnedfromghost", DontTriggerCreep)
		DontTriggerCreep(inst)	
	end
	inst:SetStateGraph(mob.stategraph)
	inst.components.talker:IgnoreAll()
	--if MONSTERHUNGER == "Disable" then
		inst.components.hunger:Pause()
	--end
	inst.components.sanity.ignore = true
	----------------------------------
	--Locomotor--
    inst.components.locomotor.runspeed = (mob.runspeed)
	inst.components.locomotor.walkspeed = (mob.walkspeed)
	
	----------------------------------
	--Loot drops--
	--inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('berniep')
	----------------------------------
	--Temperature and Weather Components--
	inst.components.temperature.maxtemp = 60 --prevents overheating
	inst.components.temperature.mintemp = 20 --prevents freezing
	inst.components.moisture:SetInherentWaterproofness(1) --Prevents getting wet.
	----------------------------------
	--Tags--
	--inst:RemoveTag("character") --This is removed to make certain mobs to not treat you like a player.
	--inst:AddTag("special_atk1") --allows taunting.
	
	inst:AddTag("woodies_waifu_for_lifeu")
	
	inst.taunt = true
	inst.mobsleep = true
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	inst.isshiny = 0
	----------------------------------
	--SanityAura--
	--inst:AddComponent("sanityaura")
	
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
	
	--Kinds of diets I know of: MEAT, ROUGHAGE(grass and stuff), GEARS, OMNI(no idea it prevents eating entirely, just disable this if you want both meat and veggies).
	--inst.components.eater:SetDiet({ FOODTYPE.MEAT, FOODTYPE.MEAT }, { FOODTYPE.MEAT, FOODTYPE.MEAT }) 
    --inst.components.eater:SetAbsorptionModifiers(2,1.5,1.5) --This might multiply food stats.
	--inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	--inst.components.eater.strongstomach = true -- can eat monster meat!
	--inst.components.eater.eatwholestack = true --should eat the entire stack and get all the benefits.
	--inst.components.eater.ignoresspoilage = true -- Can eat spoiled food without consequence. Might keep food stats as if fresh.
    --inst.components.eater:SetCanEatGears() --self explanatory.
    --inst.components.eater:SetOnEatFn(OnEat) --Runs a function when the player eats something.
	---------------------------------
	--Physics and Scale--
	
	MakeCharacterPhysics(inst, 50, .25)
	--MakeGhostPhysics(inst, 1, .5) --Special physics for flying characters(?)
    inst.Transform:SetScale(mob.scale, mob.scale, mob.scale)
    inst.DynamicShadow:SetSize(1, .5)
	inst.DynamicShadow:Enable(false) --Disables shadows.
    inst.Transform:SetSixFaced()
	---------------------------------
	--Shedder--
	
	--inst:AddComponent("shedder")
	--inst.components.shedder.shedItemPrefab = "spidergland"
	--inst.components.shedder.shedHeight = 0.1
	--inst.components.shedder:StartShedding(1200) --Note: 480 is 1 day. 480 x n = n amount of days.
	---------------------------------
	--Periodic Spawner--
	
	--inst:AddComponent("periodicspawner")
   -- inst.components.periodicspawner:SetPrefab("guano")
    --inst.components.periodicspawner:SetRandomTimes(120,240)
    --inst.components.periodicspawner:SetDensityInRange(30, 2)
    --inst.components.periodicspawner:SetMinimumSpacing(8)
    --inst.components.periodicspawner:Start()
	---------------------------------
	--Listeners--
	
	inst:ListenForEvent("equip", EquipHat) --Shows head when hats make heads disappear.
	--inst:ListenForEvent("equip", Equip) --Enables spitting.
	--inst:ListenForEvent("attacked", onattacked)
	--inst:ListenForEvent("onhitother", OnHitOther)
	---------------------------------
	--Functions that saves and loads data.
	
    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    ------------------------------------------------------
	--Light and Character Functions. Don't Touch.--
    
	inst.components.grue:AddImmunity("charlies_waifu")
	
	inst.components.talker.colour = Vector3(127/255, 0/255, 0/255) --Text color when player Examines.
	inst:ListenForEvent("respawnfromghost", RestoreNightImmunity)

    inst:DoTaskInTime(0, setChar) --Sets Character.
	inst:DoTaskInTime(3, setSkin)
    inst:ListenForEvent("respawnfromghost", function() --Runs functions when character revives from Ghost.
		inst:DoTaskInTime(3, function()  inst.components.health:SetInvincible(false)
            if inst.components.playercontroller ~= nil then
				inst.components.playercontroller:EnableMapControls(true)
				inst.components.playercontroller:Enable(true)
			end
			inst.components.inventory:Show()
			inst:ShowActions(true)
            inst:ShowHUD(true)
            inst:SetCameraDistance()
			inst.sg:RemoveStateTag("busy")
            SerializeUserSession(inst) 
		end)
        inst:DoTaskInTime(5, setChar)
		inst:DoTaskInTime(5.1, setSkin)
		inst:DoTaskInTime(6, RemovePenalty) --Removes death penalties and restore health if wanted.
		inst:DoTaskInTime(6, DontTriggerCreep)-- Restores speed on revive.
		inst:DoTaskInTime(10, RestoreNightImmunity) --explains itself.
    end)
	
    return inst
	
end




return MakePlayerCharacter("lucyp", prefabs, assets, common_postinit, master_postinit, start_inv)
