local SkinsPuppet = require "widgets/skinspuppet" --by the time this is called, it should already be populated with necessary data

--These functions exist because the ppskin_manager has no replica component, but the pp_skins table is still on the client
local function GetOwnedSkin(inst, skintable)
	if skintable and not skintable.locked and skintable.owners then
		--print("skintable has owners, iterating...")
		local isowned = false
		for i, v in ipairs(skintable.owners) do
			if inst.userid == v then
				--print("user owns the skin!")
				isowned = true
			end
		end
		return isowned
	else
		--print("skintable does not have owners, returning true")
		return not skintable.locked
	end
end

local function GetSkinsList(inst, mobtable)
	local skintable = {}
	for i, v in pairs(mobtable) do --fun fact, ipairs only works for integer keys aka not named keys, neat
		if GetOwnedSkin(inst, v) then
			table.insert(skintable, v.fname.." skin = /setskin "..v.name)
		end
	end
	if #skintable > 0 then
		inst.HUD.controls.networkchatqueue:DisplaySystemMessage("Here are skins you can use! You can also use '/setskin 0' to return to normal")
		for i, v in ipairs(skintable) do
			inst.HUD.controls.networkchatqueue:DisplaySystemMessage(v)
		end
	else
		inst.HUD.controls.networkchatqueue:DisplaySystemMessage("Sorry, but you don't have any skins for this character...")
	end
end

AddUserCommand("setskin", {
    prettyname = "Set Skin", 
    desc = "Change your skin! Use '/setskin help' for help", 
    permission = COMMAND_PERMISSION.USER,
    slash = true,
    usermenu = false,
    servermenu = false,	
    params = {"skin"},
    vote = false,
    serverfn = function(params, caller)
		print(params.skin)
		if caller ~= nil and not (caller:HasTag("playerghost") or caller:HasTag("INLIMBO") or 
		(caller.sg and caller.sg:HasStateTag("busy"))) and params.skin and caller.components.ppskin_manager and 
		SkinsPuppet.pp_skins[caller.prefab] and params.skin ~= "help" then
			--print("Intial check for setskin passed")
			caller.components.ppskin_manager:SetSkin(params.skin, caller.prefab)
		end	
    end,
	localfn = function(params, caller)
		if caller ~= nil and caller.HUD ~= nil then
			if params.skin and params.skin ~= "help" and params.skin ~= "none" and params.skin ~= "0" and params.skin ~= "default" then
				if not SkinsPuppet.pp_skins[caller.prefab] then
					caller.HUD.controls.networkchatqueue:DisplaySystemMessage("Sorry, but your character doesn't support this feature")
				elseif not SkinsPuppet.pp_skins[caller.prefab][params.skin] then
					caller.HUD.controls.networkchatqueue:DisplaySystemMessage("Sorry, but your character doesn't have this skin! Use '/setskin help' for other skins")
				elseif SkinsPuppet.pp_skins[caller.prefab][params.skin] and not GetOwnedSkin(caller, SkinsPuppet.pp_skins[caller.prefab][params.skin]) then
					caller.HUD.controls.networkchatqueue:DisplaySystemMessage("Sorry, but you don't have access to this skin! Use '/setskin help' for other skins")
				end
			end
			if (not params.skin or params.skin == "help") and SkinsPuppet.pp_skins[caller.prefab] then
				GetSkinsList(caller, SkinsPuppet.pp_skins[caller.prefab])
			end
		end	
    end,
})