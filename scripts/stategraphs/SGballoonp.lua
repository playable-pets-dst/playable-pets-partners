require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget"}
	end
end

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst) 
		if TheNet:GetServerGameMode() == "lavaarena" then
			return "attack"
		else
			return "fail"
		end	
	end),
	ActionHandler(ACTIONS.CASTAOE, "action"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("hit") end end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
	--PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnDeath(),
	PP_CommonHandlers.AddCommonHandlers(),
    --CommonHandlers.OnFreeze(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	--table.insert(events, CommonHandlers.OnHop())
end

 local states=
{

    State{
        name = "idle",
        tags = { "idle", "canrotate" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,
    },

	State{
        name = "attack",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            --inst.AnimState:PlayAnimation("pop")
			inst.components.health:Kill()
        end,

        timeline =
        {
		
        },

        events =
        {
            --EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
        name = "opengift",
        tags = { "busy", "pausepredict" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
            inst:ClearBufferedAction()

            --if IsNearDanger(inst) then
                --inst.sg.statemem.isdanger = true
                --inst.sg:GoToState("idle")
                --if inst.components.talker ~= nil then
                   -- inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                --end
                --return
           -- end

            inst.SoundEmitter:PlaySound("dontstarve/common/player_receives_gift")
            inst.AnimState:PlayAnimation("idle", true)
            -- NOTE: the previously used ripping paper anim is called "giift_loop"

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:EnableMapControls(false)
                inst.components.playercontroller:Enable(false)
            end
            inst.components.inventory:Hide()
            inst:PushEvent("ms_closepopups")
            inst:ShowActions(false)
            inst:ShowPopUp(POPUPS.GIFTITEM, true)

            if inst.components.giftreceiver ~= nil then
                inst.components.giftreceiver:OnStartOpenGift()
            end
        end,

        timeline =
        {
            -- Timing of the gift box opening animation on giftitempopup.lua
            TimeEvent(155 * FRAMES, function(inst)
               -- inst.AnimState:PlayAnimation("gift_open_pre")
                inst.AnimState:PushAnimation("idle", true)
            end),
        },

        events =
        {
            EventHandler("firedamage", function(inst)
                inst.AnimState:PlayAnimation("idle")
                inst.sg:GoToState("idle", true)
                if inst.components.talker ~= nil then
                    inst.components.talker:Say(GetString(inst, "ANNOUNCE_NODANGERGIFT"))
                end
            end),
            EventHandler("ms_doneopengift", function(inst, data)
				inst.sg:GoToState("idle", true)
                
            end),
        },

        onexit = function(inst)
            if inst.sg.statemem.isdanger then
                return
            elseif not inst.sg.statemem.isopeningwardrobe then
                if inst.components.playercontroller ~= nil then
                    inst.components.playercontroller:EnableMapControls(true)
                    inst.components.playercontroller:Enable(true)
                end
                inst.components.inventory:Show()
                inst:ShowActions(true)
            end
            inst:ShowPopUp(POPUPS.GIFTITEM, false)
        end,
    },
	
	State{
        name = "hit",
        tags = { "busy" },

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bernie/hit")
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },	
	
	State{
		name = "special_atk1",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("pop")
        end,
		
		timeline =
        {

        },


        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "action",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
			inst:PerformBufferedAction()
            inst.AnimState:PlayAnimation("hit")
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound("dontstarve/common/balloon_pop")
            inst.AnimState:PlayAnimation("pop")
			inst.components.combat:DoAreaAttack(inst, 2, nil, nil, nil, GetExcludeTags())
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("idle", true)
			
        end,
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			--inst.AnimState:PlayAnimation("idle")
        end,
		
		timeline = 
		{

		},
		
        events=
			{
				--EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("idle")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
}

--CommonStates.AddFrozenStates(states)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"hit", nil, nil, "idle", "hit") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
	PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound("dontstarve/common/balloon_pop")
				inst:RemoveTag("corpse")
			end),
		},
		
		corpse_taunt =
		{

		},
	
	},
	--anims = 
	{
		corpse = "pop",
		corpse_taunt = "hit"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{
		corpse = function(inst)
			inst.components.combat:DoAreaAttack(inst, 5, nil, nil, nil, GetExcludeTags())
		end,
	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddSailStates(states, {}, "hit", "idle")
local simpleanim = "run_pst"
local simpleidle = "idle"
local simplemove = "run"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simpleidle, loop = simpleidle, pst = simpleidle}, nil, "pop")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleidle,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleidle,
	
	plank_hop_pre = simpleidle,
	plank_hop = simpleidle,
	
	steer_pre = simpleidle,
	steer_idle = simpleidle,
	steer_turning = simpleidle,
	stop_steering = simpleidle,
	
	row = "hit",
}
)


    
return StateGraph("balloonp", states, events, "idle", actionhandlers)

