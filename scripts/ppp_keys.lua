-- Unused for now, seemed like overkill
local KEYS = {}

-- 65 is A's key code and 90 is Z's key code
-- 97 ia a's key code, 122 is z's key code
-- Don't Starve uses lowercase for key listeners, it would appear
for i = 97, 122 do 
	KEYS[string.char(i):upper()] = i
end

return KEYS