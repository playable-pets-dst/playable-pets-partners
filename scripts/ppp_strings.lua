
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------

--STRINGS.RECIPE_DESC.BATTLESTANDARD_DAMAGERP = "Increase nearby allies' damage by 25%"



------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------

STRINGS.CHARACTER_TITLES.berniep = "Bernie"
STRINGS.CHARACTER_NAMES.berniep = "BERNIE"
STRINGS.CHARACTER_DESCRIPTIONS.berniep = "*Is Bernie\n*Can do it\n*Believe in him"
STRINGS.CHARACTER_QUOTES.berniep = "Help me Bernie!"

STRINGS.CHARACTER_TITLES.abbyp = "Abigail"
STRINGS.CHARACTER_NAMES.abbyp = "ABIGAIL"
STRINGS.CHARACTER_DESCRIPTIONS.abbyp = "*Is Abigail\n*Can't do it\n*Won't stop holding F"
STRINGS.CHARACTER_QUOTES.abbyp = "Noob Abby."

STRINGS.CHARACTER_TITLES.smallghostp = STRINGS.NAMES.SMALLGHOST
STRINGS.CHARACTER_NAMES.smallghostp = STRINGS.NAMES.SMALLGHOST
STRINGS.CHARACTER_DESCRIPTIONS.smallghostp = "*Is a ghost\n*Can't attack\n*Immune to most things"
STRINGS.CHARACTER_QUOTES.smallghostp = "It can't find its lost toys."

STRINGS.CHARACTER_TITLES.maxpuppetp = "Maxwell's Minion"
STRINGS.CHARACTER_NAMES.maxpuppetp = "Maxwell's Minion"
STRINGS.CHARACTER_DESCRIPTIONS.maxpuppetp = "*Is a shadow\n*Is pretty much just a human\n*Immune to most things"
STRINGS.CHARACTER_QUOTES.maxpuppetp = "Perfect for slave labor."

STRINGS.CHARACTER_TITLES.balloonp = "Balloon"
STRINGS.CHARACTER_NAMES.balloonp = "BALLOON"
STRINGS.CHARACTER_DESCRIPTIONS.balloonp = "*1 HP\n*Does damage when popped\n*#1 Most requested thing"
STRINGS.CHARACTER_QUOTES.balloonp = "Don't let your dreams be memes."

STRINGS.CHARACTER_TITLES.balloon_partyp = "Balloon"
STRINGS.CHARACTER_NAMES.balloon_partyp = "BALLOON"
STRINGS.CHARACTER_DESCRIPTIONS.balloon_partyp = "*1 HP\n*Does damage when popped\n*Leaves confetti behind"
STRINGS.CHARACTER_QUOTES.balloon_partyp = "Why?"

STRINGS.CHARACTER_TITLES.balloon_speedp = "Balloon"
STRINGS.CHARACTER_NAMES.balloon_speedp = "BALLOON"
STRINGS.CHARACTER_DESCRIPTIONS.balloon_speedp = "*1 HP\n*Does damage when popped\n*Is pretty fast"
STRINGS.CHARACTER_QUOTES.balloon_speedp = "No really, Why?"


------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.berniep = "*Is bulky\n*Grabs aggro with X\n*Starts out big\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.balloonp = "*1 HP\n*Popping kills everything in one hit\n*Cannot be revived\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.abbyp = "*Is a ghost\n*Auto-revives\n*Revives other instantly, but loses health\n\nExpertise:\nMelee, Darts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.smallghostp = "*Is a ghost\n*Auto-revives\n*Revives other instantly, but loses health\n\nExpertise:\nDarts, Books, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.maxpuppetp = "*Is immune to debuffs\n*Brings a nightmare sword with it\n*Can't use much else.\n\nExpertise:\nMelee, Darts, Books, Staves"

------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------

STRINGS.NAMES.PMONSTER_WPN = "Mysterious Power"
STRINGS.NAMES.NIGHTSWORDP_FORGE = STRINGS.NAMES.NIGHTSWORD --This is just here so that the game will report the nightmare sword as cause of death in forge.

------------------------------------------------------------------
-- Pigman-specific speech strings
------------------------------------------------------------------

--STRINGS.CHARACTERS.PIGMANPLAYER = require "speech_pigmanplayer"

------------------------------------------------------------------
-- The default responses of characters examining the prefabs
------------------------------------------------------------------

--STRINGS.CHARACTERS.GENERIC.DESCRIBE.EVMONSTER_WPN = 
--{
	--GENERIC = "I shouldn't have this",
--}

------------------------------------------------------------------
-- Something Different skin strings
------------------------------------------------------------------

--STRINGS.SKIN_QUOTES.houndplayer_1 = "The Rare Varg Hound"
--STRINGS.SKIN_NAMES.houndplayer_1 = "Varg Hound"

--"Community"--


------------------------------------------------------------------
-- Shadow skin strings
------------------------------------------------------------------

--[[
STRINGS.SKIN_QUOTES.houndplayer_shadow = "The Rare Varg Hound"
STRINGS.SKIN_NAMES.houndplayer_shadow = "Shadow Hound"
--]]

return STRINGS